<?php
/**
 * Requests' controller
 *
 */

if(session_id() == '')
	session_start();

if(isset($_POST))
{
	if(isset($_POST['formmail']))
	{
		global $wpdb;

		$_POST['formmail']['recipients'] = json_encode(array_map('trim', explode(',', $_POST['formmail']['recipients'])));
		if(isset($_POST['formmail']['id']))
		    $wpdb->update($wpdb->prefix.'custom_formmail', $_POST['formmail'], array('id' => $_POST['formmail']['id']));
		else
		    $wpdb->insert($wpdb->prefix.'custom_formmail', $_POST['formmail']);
		wp_redirect(get_admin_url().'admin.php?page=mail-form&feedback='.urlencode('Formulário cadastrado com sucesso'));
	    die;
	}
	else if(isset($_GET['delete_formmail']))
	{
	    global $wpdb;
	    $wpdb->delete($wpdb->prefix.'custom_formmail', array('id' => $_GET['form_id']));
	    wp_redirect(get_admin_url().'admin.php?page=mail-form&feedback='.urlencode('Formulário excluído com sucesso'));
	    die;
	}
	else if(isset($_POST['mailformsettings']))
	{
		update_option('g_recaptcha_site_key', $_POST['mailformsettings']['site_key']);
		update_option('g_recaptcha_secret_key', $_POST['mailformsettings']['secret_key']);

		wp_redirect(get_admin_url().'admin.php?page=mail-form-settings&feedback='.urlencode('Configurações salvas'));
	}
	else if(isset($_POST['mailformfields']))
	{
		global $wpdb;

		foreach ($_POST['mailformfields'] as $id => $emails)
		{
			$data['recipients'] = json_encode(explode(',', $emails));
			$wpdb->update($wpdb->prefix.'custom_formmail', $data, array('id' => $id));
		}

		wp_redirect(get_admin_url().'admin.php?page=mail-form-editor&feedback='.urlencode('Alterações salvas com sucesso'));
	}
	else
	{
		$formMailDB = new FormMailDB();
		$forms      = $formMailDB->getFormsArray();
		$prefix     = null;
		foreach ($_POST as $key => $post)
		{
			if(isset($forms[$key]))
			{
				$prefix = $key;
				break;
			}
		}

		if(isset($_POST[$prefix]))
		{
		    if(FormMailValidation::reCaptcha($_POST['g-recaptcha-response']))
		    {
		        $attrs                 = $_POST[$prefix];
		        $attrs['template_url'] = get_bloginfo('template_url');

				$cont        = 0;
				$files       = $_FILES;
				$attachments = array();
		        foreach ($files as $key => $value)
		        {
					$file = $files[$key];
		        	if(isset($file) and !empty(array_filter($file['tmp_name'])))
			        {
			            $ext           = explode('.', $file['name']['file']);
			            $ext           = end($ext);
			            $upload_dir    = wp_upload_dir();
			            $fileTemp      = $upload_dir['basedir'].'/temp/attachment-'.$cont.'.'.$ext;
			            move_uploaded_file($file['tmp_name']['file'], $fileTemp);
			            $attachments[] = $fileTemp;
			            $cont++;
			        }
		        }

		        $subject = (isset($_POST[$prefix]['subject']) and strlen($_POST[$prefix]['subject'])) ? $_POST[$prefix]['subject'] : $forms[$prefix]['title'].' - '.get_bloginfo('name');

		        $mail = new Mail();
		        $mail->setTemplate(get_template_directory().'/'.$forms[$prefix]['template'])
		             ->setFrom($attrs['email'])
		             ->setFromName($attrs['name'])
		             ->setSubject($subject)
		             ->setAttributes($attrs)
		             ->setRecipients($forms[$prefix]['recipients'])
		             ->setAttachments($attachments)
		             ->send();

		        if(!empty($attachments))
		        {
		            foreach ($attachments as $fileTemp)
		                unlink($fileTemp);
		        }

		        FormMailHelper::feedback('success', $forms[$prefix]['feedback']);
		    }
		    else
		        FormMailHelper::feedback('error', 'Por favor, para sua segurança selecione campo "Eu não sou robô"');

		    $redirect = isset($_POST[$prefix]['redirect']) ? $_POST[$prefix]['redirect'] : get_bloginfo('url');
		    wp_redirect($redirect);
		    die;
		}
	}
}
