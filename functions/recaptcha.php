<?php

wp_enqueue_script( 'forms',  dirname( plugin_dir_url( __FILE__ ) ). '/views/assets/js/recaptcha.js', array( 'jquery' ), false, true );
wp_localize_script( 'forms', 'form',
  array(
    'site_key'      => get_option('g_recaptcha_site_key'),
    'template_url'  => get_bloginfo('template_url'),
    'date_server'   => date("Y-m-d")
  )
);

// add CAPTCHA header script to WordPress header
add_action( 'wp_head', 'header_script' );
/** reCAPTCHA header script */
function header_script() {
    echo '<script src="https://www.google.com/recaptcha/api.js?hl=pt-BR&onload=loadCaptcha&render=explicit" async defer></script>';
}
