jQuery( document ).ready(function(){
    init();
});

var init = function() {

    recaptcha = [];

    jQuery('.re-captcha').each( function( index ) {
        recaptcha[index] = [];
        recaptcha[index]["container"] = null;
        recaptcha[index]["response"] = null;

        recaptcha[index]["key"] = jQuery(this).children().attr('id');
        recaptcha[index]["custom_conf"] = jQuery( "#" + recaptcha[index]["key"] ).data();
        recaptcha[index]["default_conf"] = {
            'sitekey' : form.site_key,
            'callback' : function(response) {
              recaptcha[index]["response"] = response;
              jQuery( "#" + recaptcha[index]["key"] ).next().removeClass('show');
            }
        };
        jQuery.extend( recaptcha[index]["default_conf"], recaptcha[index]["custom_conf"] );
    });

    loadCaptcha = function() {
        $.each( recaptcha, function( index, value) {
            if( !isNaN( index ) ) {
                grecaptcha.render( value['key'], value['default_conf'] );
                jQuery( "#" + value['key'] ).closest( ".forms" ).submit( function() {
                    if( value['response'] != null ){
                        return true;
                    }
                    jQuery( "#" + value['key'] ).next().addClass('show');
                    return false;
                });
            }
        });
    };
}
