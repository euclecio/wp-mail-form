<?php
/**
 * Class developed to send mails using Wordpress
 *
 * @author Euclécio Josias Rodrigues <eucjosias@gmail.com>
 *
 * @version 1.0
 *
 */

Class Mail
{
	/**
	 * @param string
	 *
	 * Path for template of email
	 *
	 */
	private $template;

	/**
	 * @param string
	 *
	 */
	private $from;

    /**
     * @param string
     *
     */
    private $fromName;

	/**
	 * @param string
	 *
	 */
	private $subject;

	/**
	 * @param array
	 *
	 * Array of recipients
	 *
	 */
	private $recipients = array();

	/**
	 * @param array
	 *
	 * Array of attachments
	 *
	 */
	private $attachments = array();

	/**
	 * @param array
	 *
	 * Array of attributes
	 *
	 */
	private $attributes = array();

	/**
     * Gets the value of template.
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Sets the value of template.
     *
     * @param mixed $template the template
     *
     * @return self
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Gets the value of from.
     *
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Sets the value of fromName.
     *
     * @param mixed $fromName the fromName
     *
     * @return self
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;

        return $this;
    }

    /**
     * Gets the value of fromName.
     *
     * @return string
     */
    public function getFromName()
    {
        return $this->fromName;
    }

    /**
     * Sets the value of from.
     *
     * @param mixed $from the from
     *
     * @return self
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Gets the value of subject.
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Sets the value of subject.
     *
     * @param mixed $subject the subject
     *
     * @return self
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Gets the value of recipients.
     *
     * @return array
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * Sets the value of recipients.
     *
     * @param mixed $recipients the recipients
     *
     * @return self
     */
    public function setRecipients(Array $recipients)
    {
        $this->recipients = $recipients;

        return $this;
    }

    /**
     * Add value in recipients array.
     *
     * @param $key => $value in recipients array
     *
     * @return self
     */
    public function addTo($to)
    {
        $this->recipients[] = $to;

        return $this;
    }

    /**
     * Gets the value of attachments.
     *
     * @return array
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Sets the value of attachments.
     *
     * @param mixed $attachments the attachments
     *
     * @return self
     */
    public function setAttachments(Array $attachments)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * Add value in attachments array.
     *
     * @param $key => $value in attachments array
     *
     * @return self
     */
    public function addAttachment($attachment)
    {
        $this->attachments[] = $attachment;

        return $this;
    }

    /**
     * Gets the value of attributes.
     *
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Sets the value of attributes.
     *
     * @param mixed $attributes the attributes
     *
     * @return self
     */
    public function setAttributes(Array $attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * Add value in attributes array.
     *
     * @param $key => $value in attributes array
     *
     * @return self
     */
    public function addAttribute($key, $value)
    {
        $this->attributes[$key] = $value;

        return $this;
    }

    /**
     * This function reads the recipients array and send emails
     *
     * @param $redirecTo string
     *
     */
    public function send()
	{
		require_once("PHPMailer/class.phpmailer.php");
        $mail = new PHPMailer();

        /* SMTP Config */
        $mail->IsSMTP();
        $mail->Host       = MAILSERVER_URL;
        $mail->SMTPAuth   = true;
        $mail->CharSet 	  = 'UTF-8';
        $mail->Username   = MAILSERVER_LOGIN;
        $mail->Password   = MAILSERVER_PASS;
        $mail->SMTPSecure = "tls";
        $mail->Port       = 587;

        /* From vars */
        $mail->From     = $this->from;
        $mail->FromName = $this->fromName;
        $mail->AddReplyTo($this->from, $this->fromName);

        /* Add recipients */
        foreach ($this->recipients as $to)
            $mail->AddAddress($to);

        $mail->WordWrap = 50;

        /* Add attachments */
        if (count($this->attachments))
        {
            foreach ($this->attachments as $filePath)
                $mail->AddAttachment($filePath);
        }

        /* HTML content */
        $mail->IsHTML(true);

        /* Email Body */
        $message       = $this->replaceTemplateKeys();
        $mail->Subject = $this->subject;
        $mail->Body    = $message;
        $mail->AltBody = strip_tags($message);

        /* Send */
        if(!$mail->Send())
        {
            echo "Message could not be sent. <p>";
            echo "Mailer Error: " . $mail->ErrorInfo;
            exit;
        }
	}

	/**
	 * This function replaces keys in template
	 *
	 * @param array $attributes
	 *
	 * @return string
	 */
	public function replaceTemplateKeys()
	{
		if( file_exists( $this->template ) ) {
			ob_start();
			require( $this->template );
			$rendered = ob_get_contents();
			ob_end_clean();
			return $rendered;
		}
	}
}
